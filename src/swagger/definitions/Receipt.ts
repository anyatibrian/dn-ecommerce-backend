module.exports = {
  Receipt: {
    type: 'object',
    required: ['id', 'userId'],
    properties: {
      id: {
        type: 'string',
        enum: ['UUID'],
      },
      userId: {
        type: 'string',
        enum: ['UUID'],
      },
      orderId: {
        type: 'string',
        enum: ['UUID'],
      },
      invoiceNumber: {
        type: 'string',
        format: 'integer',
      },
      amountToBePaid: {
        type: 'number',
        format: 'integer',
        example: 50,
      },
      paymentStatus: {
        type: 'string',
        enum: ['paid', 'pending'],
        example: 'pending',
      },
      order: {
        id: {
          type: 'string',
          enum: ['UUID'],
        },
        userId: {
          type: 'string',
          enum: ['UUID'],
        },
        amount: {
          type: 'number',
          format: 'integer',
          minimum: 1,
          example: 10,
        },
        quantity: {
          type: 'number',
          format: 'integer',
          minimum: 1,
          example: 10,
        },
        orderItems: {
          type: 'array',
          format: 'array of objects',
          example: [
            {
              productId: 'UUID',
              quantity: 1,
            },
          ],
        },
        status: {
          type: 'string',
          enum: ['paid', 'pending', 'aborted'],
        },
        comments: { type: 'string' },
        createdAt: {
          type: 'date',
          format: 'date-time',
        },
        updatedAt: {
          type: 'date',
          format: 'date-time',
        },
      },
      createdAt: {
        type: 'date',
        format: 'date-time',
      },
      updatedAt: {
        type: 'date',
        format: 'date-time',
      },
    },
  },
}
